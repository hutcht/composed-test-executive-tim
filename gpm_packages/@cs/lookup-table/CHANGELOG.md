# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.1.0] 2019-06-13
### Added
- Table lookup key can be any data type
- Public API method to flatten any value to string key (Only call this method external to Read/Write if you need to leak the flattened key to the caller. For normal use, just wire any key directly to public Read/Write methods.)

## [0.0.3] - 2019-02-26
### Fixed
- Writing as variant now skips element type check (previously all variants failed overwrite)

## [0.0.2] - 2019-02-14
### Removed
- All tests and project file excluded from package

## [0.0.1] - 2019-02-07
### Changed
- Performance optimization when deleting an item without type check

## [0.0.0] - 2019-02-05
### Added
- Everything (Initial publication)